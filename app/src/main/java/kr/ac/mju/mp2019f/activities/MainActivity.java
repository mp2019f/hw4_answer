package kr.ac.mju.mp2019f.activities;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.ByteArrayOutputStream;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private ImageView iv_doosan;
    private TextView tv_doosan_likesnum;
    private TextView tv_doosan_dislikesnum;

    private ImageView iv_lg;
    private TextView tv_lg_likesnum;
    private TextView tv_lg_dislikesnum;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        iv_doosan = findViewById(R.id.iv_doosan);
        tv_doosan_likesnum = findViewById(R.id.tv_doosan_likesnum);
        tv_doosan_dislikesnum = findViewById(R.id.tv_doosan_dislikesnum);

        iv_lg = findViewById(R.id.iv_lg);
        tv_lg_likesnum = findViewById(R.id.tv_lg_likesnum);
        tv_lg_dislikesnum = findViewById(R.id.tv_lg_dislikesnum);

        tv_doosan_likesnum.setText("0");
        tv_doosan_dislikesnum.setText("0");

        tv_lg_likesnum.setText("0");
        tv_lg_dislikesnum.setText("0");


        iv_doosan.setOnClickListener(this);
        iv_lg.setOnClickListener(this);



    }

    @Override
    public void onClick(View view) {

        Intent intent = new Intent(this,TeamActivity.class);

        switch (view.getId()){
            case R.id.iv_doosan:

                String str_doosan_like = tv_doosan_likesnum.getText().toString();
                String str_doosan_dislike = tv_doosan_dislikesnum.getText().toString();

                Bitmap sendLogo = BitmapFactory.decodeResource(getResources(),R.drawable.seoul_doosan);
                ByteArrayOutputStream streamlogo = new ByteArrayOutputStream();
                sendLogo.compress(Bitmap.CompressFormat.PNG, 100, streamlogo);
                byte[] byteArray = streamlogo.toByteArray();
                intent.putExtra("teamlogo",byteArray);

                intent.putExtra("teamname","DOOSAN BEARS");
                intent.putExtra("teambirth","1982 / 01 / 15");
                intent.putExtra("teamwin","V 5");
                intent.putExtra("teamcompany","DOOSAN");

                intent.putExtra("like",str_doosan_like);
                intent.putExtra("dislike",str_doosan_dislike);

                break;

            case R.id.iv_lg:

                String str_lg_like = tv_lg_likesnum.getText().toString();
                String str_lg_dislike = tv_lg_dislikesnum.getText().toString();

                Bitmap sendLogo2 = BitmapFactory.decodeResource(getResources(),R.drawable.seoul_lg);
                ByteArrayOutputStream streamlogo2 = new ByteArrayOutputStream();
                sendLogo2.compress(Bitmap.CompressFormat.PNG, 100, streamlogo2);
                byte[] byteArray2 = streamlogo2.toByteArray();
                intent.putExtra("teamlogo",byteArray2);

                intent.putExtra("teamname","LG TWINS");
                intent.putExtra("teambirth","1990 / 03 / 15");
                intent.putExtra("teamwin","V 2");
                intent.putExtra("teamcompany","LG");

                intent.putExtra("like",str_lg_like);
                intent.putExtra("dislike",str_lg_dislike);

                break;
        }

        startActivityForResult(intent,5000);

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        //super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == 250){
            switch (requestCode){
                case 5000:


                    tv_doosan_likesnum.setText(data.getStringExtra("dslike"));
                    tv_doosan_dislikesnum.setText(data.getStringExtra("dsdislike"));

                    break;
            }
        }else if(resultCode == 500){
            switch (requestCode){
                case 5000:

                    tv_lg_likesnum.setText(data.getStringExtra("lglike"));
                    tv_lg_dislikesnum.setText(data.getStringExtra("lgdislike"));

                    break;
            }
        }
    }
}
