package kr.ac.mju.mp2019f.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class TeamActivity extends AppCompatActivity implements View.OnClickListener {

    private ImageView iv_teamlogo;
    private TextView tv_teamname;
    private TextView tv_teambirth;
    private TextView tv_teamwin;
    private TextView tv_teamcompany;

    private Button bt_like;
    private Button bt_dislike;
    private TextView tv_like;
    private TextView tv_dislike;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_team);

        iv_teamlogo = findViewById(R.id.iv_teamlogo);
        tv_teamname = findViewById(R.id.tv_teamname);
        tv_teambirth = findViewById(R.id.tv_teamday);
        tv_teamwin = findViewById(R.id.tv_teamwin);
        tv_teamcompany = findViewById(R.id.tv_teamcompanyname);

        bt_like = findViewById(R.id.bt_like);
        bt_dislike = findViewById(R.id.bt_dislike);
        tv_like = findViewById(R.id.tv_like);
        tv_dislike = findViewById(R.id.tv_dislike);


        bt_like.setOnClickListener(this);
        bt_dislike.setOnClickListener(this);


        Bundle extras = getIntent().getExtras();

        String str_team = extras.getString("teamname");

        if(str_team.equals("DOOSAN BEARS")){

            byte[] arr = getIntent().getByteArrayExtra("teamlogo");
            Bitmap bitmap = BitmapFactory.decodeByteArray(arr,0,arr.length);

            iv_teamlogo.setImageBitmap(bitmap);


            String str_teamname = extras.getString("teamname");
            String str_teambirth = extras.getString("teambirth");
            String str_teamwin = extras.getString("teamwin");
            String str_teamcompany = extras.getString("teamcompany");

            String str_like = extras.getString("like");
            String str_dislike = extras.getString("dislike");


            tv_teamname.setText(str_teamname);
            tv_teambirth.setText(str_teambirth);
            tv_teamwin.setText(str_teamwin);
            tv_teamcompany.setText(str_teamcompany);

            tv_like.setText(str_like);
            tv_dislike.setText(str_dislike);

        }else if(str_team.equals("LG TWINS")){

            byte[] arr = getIntent().getByteArrayExtra("teamlogo");
            Bitmap bitmap = BitmapFactory.decodeByteArray(arr,0,arr.length);

            iv_teamlogo.setImageBitmap(bitmap);


            String str_teamname = extras.getString("teamname");
            String str_teambirth = extras.getString("teambirth");
            String str_teamwin = extras.getString("teamwin");
            String str_teamcompany = extras.getString("teamcompany");

            String str_like = extras.getString("like");
            String str_dislike = extras.getString("dislike");

            tv_teamname.setText(str_teamname);
            tv_teambirth.setText(str_teambirth);
            tv_teamwin.setText(str_teamwin);
            tv_teamcompany.setText(str_teamcompany);

            tv_like.setText(str_like);
            tv_dislike.setText(str_dislike);
        }
    }

    @Override
    public void onClick(View view) {
        String str_team_check = tv_teamname.getText().toString();

        if(str_team_check.equals("DOOSAN BEARS")){

            Intent resultIntent = new Intent();


            String str_dslike = tv_like.getText().toString();
            String str_dsdislike = tv_dislike.getText().toString();

            switch (view.getId()){
                case R.id.bt_like:
                    int num_dslike = Integer.parseInt(str_dslike);
                    num_dslike = num_dslike + 1;
                    str_dslike = Integer.toString(num_dslike);
                    tv_like.setText(str_dslike);
                    tv_dislike.setText(str_dsdislike);

                    resultIntent.putExtra("dslike",str_dslike);
                    resultIntent.putExtra("dsdislike",str_dsdislike);

                    break;

                case R.id.bt_dislike:
                    int num_dsdislike = Integer.parseInt(str_dsdislike);
                    num_dsdislike = num_dsdislike + 1;
                    str_dsdislike = Integer.toString(num_dsdislike);
                    tv_dislike.setText(str_dsdislike);
                    tv_like.setText(str_dslike);

                    resultIntent.putExtra("dsdislike",str_dsdislike);
                    resultIntent.putExtra("dslike",str_dslike);

                    break;
            }

            setResult(250,resultIntent);
            finish();

        } else if(str_team_check.equals("LG TWINS")){

            Intent resultIntent = new Intent();

            String str_lglike = tv_like.getText().toString();
            String str_lgdislike = tv_dislike.getText().toString();


            switch (view.getId()){
                case R.id.bt_like:
                    int num_lglike = Integer.parseInt(str_lglike);
                    num_lglike = num_lglike + 1;
                    str_lglike = Integer.toString(num_lglike);
                    tv_like.setText(str_lglike);
                    tv_dislike.setText(str_lgdislike);

                    resultIntent.putExtra("lglike",str_lglike);
                    resultIntent.putExtra("lgdislike",str_lgdislike);
                    break;
                case R.id.bt_dislike:
                    int num_lgdislike = Integer.parseInt(str_lgdislike);
                    num_lgdislike = num_lgdislike + 1;
                    str_lgdislike = Integer.toString(num_lgdislike);
                    tv_dislike.setText(str_lgdislike);
                    tv_like.setText(str_lglike);

                    resultIntent.putExtra("lgdislike",str_lgdislike);
                    resultIntent.putExtra("lglike",str_lglike);
                    break;
            }
            setResult(500,resultIntent);
            finish();
        }
    }
}
